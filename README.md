**Vueauth version 2 with user management**

_Laravel 8 | Php ^7.4_

_(Vue SPA, Laravel Sanctum, Spatie Roles and Permissions)_

**How to install**


1. clone this project
2.  composer install
    npm install
3. cp .env.example .env
4. **config your database connection and SANCTUM_STATEFUL_DOMAINS in env file** [very important]
5. php artisan migrate
6. php artisan db:seed
7. php artisan optimize
8. php artisan optimize:clear
9. npm run dev

**Credentials**

_default superadmin_

user:sa@test.com

pass: password

_default admin_

user : admin@test.com

pass : password

