<?php

namespace App\Providers;

use App\Interfaces\RegisterServiceInterface;
use App\Services\RegisterApiService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RegisterServiceInterface::class, function ($app) {
            return $app->make(RegisterApiService::class);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
